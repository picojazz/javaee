package components;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Adresse {
	@Column(length=30)
	private String pays;
	private String ville;
	private int codePostal;
	public Adresse(String pays, String ville, int codePostal) {
		super();
		this.pays = pays;
		this.ville = ville;
		this.codePostal = codePostal;
	}
	public Adresse() {
		super();
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public int getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}
	@Override
	public String toString() {
		return "Adresse [pays=" + pays + ", ville=" + ville + ", codePostal="
				+ codePostal + "]";
	}
	
}
