package dao;

import java.util.List;

import entities.Produit;

public interface Iproduit {
	public Produit ajouter(Produit p);
	public Produit modifier(Produit p);
	public void supprimer(Long id);
	public List<Produit> lister();
	public Produit chercherParId(Long id);
	public List<Produit> chercherParDesignation(String designation);
}
