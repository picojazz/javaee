package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import entities.Artiste;

public class IArtisteImpl implements IArtiste{
	EntityManager em = Persistence.createEntityManagerFactory("jpa").createEntityManager();
	EntityTransaction et = em.getTransaction();

	@Override
	public Artiste AddArtiste(Artiste a) {
		et.begin();
		em.persist(a);
		et.commit();
		return a;
	}
	
}
