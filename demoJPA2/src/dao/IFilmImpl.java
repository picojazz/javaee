package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import entities.Film;

public class IFilmImpl implements IFilm{
	EntityManager em = Persistence.createEntityManagerFactory("jpa").createEntityManager();
	EntityTransaction et = em.getTransaction();

	@Override
	public Film addFilm(Film f) {
		et.begin();
		em.persist(f);
		et.commit();
		return null;
	}

}
