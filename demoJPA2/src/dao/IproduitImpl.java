package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;






import entities.Produit;
@Transactional
public class IproduitImpl implements Iproduit{

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa");
	EntityManager em = emf.createEntityManager();
	EntityTransaction et = em.getTransaction();
	@Override
	public Produit ajouter(Produit p) {
		et.begin();
		em.persist(p);
		et.commit();
		return p;
	}

	@Override
	public Produit modifier(Produit p) {
		et.begin();
		em.merge(p);
		et.commit();
		return p;
	}

	

	@Override
	public List<Produit> lister() {
		
		Query req= em.createQuery("select p from Produit p") ;
		return req.getResultList();
	}

	@Override
	public Produit chercherParId(Long id) {
		Produit p = em.find(Produit.class, id);
		return p;
	}

	@Override
	public List<Produit> chercherParDesignation(String designation) {
		Query req = em.createQuery("select p from Produit p where p.designation like :x");
		req.setParameter("x", "%"+designation+"%");
		 
		return req.getResultList();
	}

	@Override
	public void supprimer(Long id) {
		et.begin();
		Produit p = em.find(Produit.class, id);
		em.remove(p);
		et.commit();
		
	}
	
}
