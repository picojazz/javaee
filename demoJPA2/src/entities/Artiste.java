package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import components.Adresse;

@Entity
public class Artiste {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String prenom;
	private Date naissance;
	@OneToMany(mappedBy="realisateur")
	private List<Film> films = new ArrayList<Film>();
	@Embedded
	private Adresse adresse;
	
	
	public Artiste() {
		super();
	}


	public Artiste(String nom, String prenom, Date naissance,
			Adresse adresse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.naissance = naissance;
		
		this.adresse = adresse;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public Date getNaissance() {
		return naissance;
	}


	public void setNaissance(Date naissance) {
		this.naissance = naissance;
	}


	public List<Film> getFilms() {
		return films;
	}


	public void setFilms(List<Film> films) {
		this.films = films;
	}


	public Adresse getAdresse() {
		return adresse;
	}


	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}


	public void addFilm(Film f){
		f.setRealisateur(this);
		this.films.add(f);
	}
	
}
