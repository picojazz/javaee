package entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Film {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String titre;
	private Date annee;
	@ManyToOne
	@JoinColumn(name="idRealisateur")
	private Artiste realisateur;
	public Film(String titre, Date annee, Artiste realisateur) {
		super();
		this.titre = titre;
		this.annee = annee;
		this.realisateur = realisateur;
	}
	public Film() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public Date getAnnee() {
		return annee;
	}
	public void setAnnee(Date annee) {
		this.annee = annee;
	}
	public Artiste getRealisateur() {
		return realisateur;
	}
	public void setRealisateur(Artiste realisateur) {
		this.realisateur = realisateur;
	}
	@Override
	public String toString() {
		return "Film [id=" + id + ", titre=" + titre + ", annee=" + annee
				+ ", realisateur=" + realisateur + "]";
	}
	

}
