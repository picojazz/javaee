package metier;

public class Client {

	private int id;
	private String nom;
	private String email;
	private String ville;
	public Client() {
		super();
	}
	public Client(int id, String nom, String email, String ville) {
		super();
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.ville = ville;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	@Override
	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", email=" + email
				+ ", ville=" + ville + "]";
	}
	
}
