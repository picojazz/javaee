package metier;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetierImpl {
	
	public static List<Client> search(String mot){
		List<Client> lcl = new ArrayList<>();
		Connection conn = DbConnect.getConn();
		try {
			PreparedStatement ps = conn.prepareStatement("Select * from clients where nom like ? or email like ? or ville like ?");
			ps.setString(1, "%"+mot+"%");
			ps.setString(2, "%"+mot+"%");
			ps.setString(3, "%"+mot+"%");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Client cl = new Client();
				cl.setId(rs.getInt("id"));
				cl.setNom(rs.getString("nom"));
				cl.setEmail(rs.getString("email"));
				cl.setVille(rs.getString("ville"));
				lcl.add(cl);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lcl;
		
	}
	public static void addClient(String nom , String email, String ville){
		try {
			Connection conn = DbConnect.getConn();
			PreparedStatement ps = conn.prepareStatement("insert into clients (nom,email,ville)values(?,?,?)");
			ps.setString(1, nom);
			ps.setString(2, email);
			ps.setString(3, ville);
			ps.executeUpdate();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

}
