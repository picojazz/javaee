package web;

import java.util.List;

import metier.Client;

public class ClientModele {
	private String motcle;
	private List<Client> clients;
	
	public ClientModele() {
		super();
	}
	public ClientModele(String motcle, List<Client> clients) {
		super();
		this.motcle = motcle;
		this.clients = clients;
	}
	public String getMotcle() {
		return motcle;
	}
	public void setMotcle(String motcle) {
		this.motcle = motcle;
	}
	public List<Client> getClients() {
		return clients;
	}
	public void setClients(List<Client> clients) {
		this.clients = clients;
	}
	
}
