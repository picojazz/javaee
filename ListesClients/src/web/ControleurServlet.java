package web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import metier.MetierImpl;

public class ControleurServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action != null){
			if(action.equals("rechercher")){
				String motcle = request.getParameter("motcle");
				List<metier.Client> lcl = new ArrayList<>();
				lcl = MetierImpl.search(motcle);
				ClientModele  mode = new ClientModele(motcle,lcl);
				request.setAttribute("mode", mode);
				request.getRequestDispatcher("client.jsp").forward(request, response);
			}else if(action.equals("ajout")){
				String nom = request.getParameter("nom");
				String email = request.getParameter("email");
				String ville = request.getParameter("ville");
				MetierImpl.addClient(nom, email, ville);
				response.sendRedirect("client?motcle=&action=rechercher");
			}
		}else{
		
		List<metier.Client> lcl = new ArrayList<>();
		lcl = MetierImpl.search("");
		ClientModele  mode = new ClientModele("",lcl);
		request.setAttribute("mode", mode);
		
		request.getRequestDispatcher("client.jsp").forward(request, response);
		}
		
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 
			
		
			doGet(request, response);
		
		
	}

}
