<%@taglib uri ="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="static/css/style.css">
<title>Client</title>
</head>
<body>
	
	<table>
		<tr>
		<form action="client" method="get">
			<td>Mot Cle : </td>
			<td><input type="text" value="${mode.motcle }"  name="motcle" /> </td>
			<td><input type="submit" value="rechercher" name="action"/></td>
			</from>
		</tr>
	</table>
	<br>
	<h1>ajout d'un client</h1>
	<form action="client" method="post">
		<table>
			<tr>
				<td>nom</td>
				<td><input type="text" name="nom"></td>
			</tr>
			<tr>
				<td>email</td>
				<td><input type="text" name="email"></td>
			</tr>
			<tr>
				<td>ville</td>
				<td><input type="text" name="ville"></td>
			</tr>
			<tr>
				<td><input type="submit" value="ajout" name="action"></td>
			</tr>
		</table>
	</form>
	<br>
	<h1>listes des clients</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Nom</tdh>
			<th>email</th>
			<th>ville</th>
		</tr>
		<c:forEach items="${mode.clients}" var="cl">
		<tr>
			<td>${cl.id}</td>
			<td>${cl.nom}</td>
			<td>${cl.email}</td>
			<td>${cl.ville}</td>
		</tr>
		</c:forEach>
	</table>
</body>
</html>