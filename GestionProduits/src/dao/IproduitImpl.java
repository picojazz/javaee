package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IproduitImpl implements Iproduit {
	Connection conn = DbConnect.getConn();
	@Override
	public  List<Produit> search(String mot) {
		List<Produit> produits = new ArrayList<Produit>();
		try {
			PreparedStatement ps =conn.prepareStatement("SELECT * FROM produits where des like ?");
			ps.setString(1, "%"+mot+"%");
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Produit p = new Produit();
				p.setId(rs.getInt("id"));
				p.setDes(rs.getString("des"));
				p.setPu(rs.getInt("pu"));
				p.setQte(rs.getInt("qte"));
				produits.add(p);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		return produits;
	}

	@Override
	public Produit add(Produit p) {
		int id = 0;
		Produit p1 = new Produit();
		try {
			PreparedStatement ps = conn.prepareStatement("INSERT INTO produits (des,pu,qte)VALUES(?,?,?)");
			ps.setString(1, p.getDes());
			ps.setInt(2, p.getPu());
			ps.setInt(3, p.getQte());
			ps.executeUpdate();
			
			PreparedStatement ps1 = conn.prepareStatement("SELECT max(id) as maxid FROM produits");
			ResultSet rs1 = ps1.executeQuery();
			if(rs1.next()){
				id = rs1.getInt("maxid");
			}
			
			PreparedStatement ps2 = conn.prepareStatement("SELECT * FROM produits WHERE id = "+id+"");
			ResultSet rs2 = ps2.executeQuery();
			if(rs2.next()){
				p1.setId(rs2.getInt("id"));
				p1.setDes(rs2.getString("des"));
				p1.setPu(rs2.getInt("pu"));
				p1.setQte(rs2.getInt("qte"));
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		
		return p1;
	}

	@Override
	public Produit edit(Produit p) {
		
		
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE produits SET des = ? ,pu = ? ,qte = ? WHERE id = ?");
			ps.setString(1, p.getDes());
			ps.setInt(2, p.getPu());
			ps.setInt(3, p.getQte());
			ps.setInt(4, p.getId());
			ps.executeUpdate();
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		return p;
	}

	@Override
	public void delete(int id) {
		
		try {
			PreparedStatement ps = conn.prepareStatement("DELETE FROM produits WHERE id = ?");
			ps.setInt(1, id);
			ps.executeUpdate();
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
	}

	@Override
	public Produit getProduitId(int id) {
		Produit p = new Produit();
		PreparedStatement ps2;
		try {
			ps2 = conn.prepareStatement("SELECT * FROM produits WHERE id = "+id+"");
		
		ResultSet rs2 = ps2.executeQuery();
		if(rs2.next()){
			p.setId(rs2.getInt("id"));
			p.setDes(rs2.getString("des"));
			p.setPu(rs2.getInt("pu"));
			p.setQte(rs2.getInt("qte"));
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p;
	}

}
