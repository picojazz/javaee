package dao;

import java.util.List;

public interface Iproduit {
	public List<Produit> search(String mot);
	public Produit add(Produit p);
	public Produit edit(Produit p);
	public void delete(int id);
	public Produit getProduitId(int id);

}
