package dao;

public class Produit {
	private int id;
	private String des;
	private int pu;
	private int qte;
	public Produit(int id, String des, int pu, int qte) {
		super();
		this.id = id;
		this.des = des;
		this.pu = pu;
		this.qte = qte;
	}
	public Produit() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public int getPu() {
		return pu;
	}
	public void setPu(int pu) {
		this.pu = pu;
	}
	public int getQte() {
		return qte;
	}
	public void setQte(int qte) {
		this.qte = qte;
	}
	@Override
	public String toString() {
		return "Produit [id=" + id + ", des=" + des + ", pu=" + pu + ", qte="
				+ qte + "]";
	}
	

}
