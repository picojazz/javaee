package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;

import dao.Iproduit;
import dao.IproduitImpl;
import dao.Produit;

public class ControleurServlet extends HttpServlet{
	private Iproduit metier;
	private ProduitModele model;
	@Override
	public void init() throws ServletException {
		
		 metier = new IproduitImpl();
		 model = new ProduitModele();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();
		if (path.equals("/produit.do")) {
			model.setMotcle("");
			model.setProduits(metier.search(""));
			request.setAttribute("model", model);
			request.getRequestDispatcher("produits.jsp").forward(request, response);
		}else if (path.equals("/search.do")) {
			String motcle = request.getParameter("motcle");
			model.setMotcle(motcle);
			model.setProduits(metier.search(motcle));
			request.setAttribute("model", model);
			request.getRequestDispatcher("produits.jsp").forward(request, response);
		}else if (path.equals("/saisie.do")) {
			request.getRequestDispatcher("ajout.jsp").forward(request, response);
			
		}else if (path.equals("/ajout.do")) {
			Produit p = new Produit();
			p.setDes(request.getParameter("des"));
			p.setPu(Integer.parseInt(request.getParameter("pu")));
			p.setQte(Integer.parseInt(request.getParameter("qte")));
			Produit produit = metier.add(p);
			request.setAttribute("produit", produit);
			request.getRequestDispatcher("confirmation.jsp").forward(request, response);
			
		}else if (path.equals("/supp.do")) {
			metier.delete(Integer.parseInt(request.getParameter("id")));
			response.sendRedirect("search.do?motcle=");
			
		}else if (path.equals("/edit.do")) {
			Produit p = metier.getProduitId(Integer.parseInt(request.getParameter("id")));
			request.setAttribute("p", p);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			
		}else if (path.equals("/edition.do")) {
			Produit p = new Produit();
			p.setId(Integer.parseInt(request.getParameter("id")));
			p.setDes(request.getParameter("des"));
			p.setPu(Integer.parseInt(request.getParameter("pu")));
			p.setQte(Integer.parseInt(request.getParameter("qte")));
			Produit produit = metier.edit(p);
			request.setAttribute("produit", produit);
			request.getRequestDispatcher("confirmation.jsp").forward(request, response);
			
		}else{
			response.sendError(Response.SC_NOT_FOUND);
		}
		
		
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
