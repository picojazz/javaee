package web;

import java.util.List;

import dao.Produit;

public class ProduitModele {
	private String motcle;
	private List<Produit> produits;
	
	public ProduitModele() {
		super();
	}
	public ProduitModele(String motcle, List<Produit> produits) {
		super();
		this.motcle = motcle;
		this.produits = produits;
	}
	public String getMotcle() {
		return motcle;
	}
	public void setMotcle(String motcle) {
		this.motcle = motcle;
	}
	public List<Produit> getProduits() {
		return produits;
	}
	public void setProduits(List<Produit> produits) {
		this.produits = produits;
	}
	
}
