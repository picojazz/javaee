
<!DOCTYPE html  >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
</head>
<body>
<%@include file="menu.jsp" %>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-primary ">
				<div class="panel-heading">Ajout d'un Produit</div>
					<div class="panel-body">
					<form action="ajout.do" method="post">
						<div class="form-group">
							<label>Designation</label>
							<input type="text" name="des" class="form-control">
						</div>
						<div class="form-group">
							<label>Prix Unitaire</label>
							<input type="text" name="pu" class="form-control">
						</div>
						<div class="form-group">
							<label>Quantite</label>
							<input type="text" name="qte" class="form-control">
						</div>
						<input type="submit" value="Ajouter" class="btn btn-lg btn-primary">

						</form>

					
					</div>
				</div>
			</div>
		</div>
</body>
</html>