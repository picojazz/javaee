<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Insert title here</title>
</head>
<body>
	
	<%@include file="menu.jsp" %>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-primary ">
				<div class="panel-heading">Liste des Produits</div>
					<div class="panel-body">
					<form class="form-inline" action="search.do" method="get">
						<div class="form-group">
							<label> Mot cle : </label>
							<input type="text" name="motcle" value="${model.motcle }" class="form-control">
							<input type="submit" value="chercher" class="btn btn-primary">
						</div>
					</form><br>
					<table class="table table-striped">
						<tr>
							<th>ID</th>
							<th>Designation</th>
							<th>Prix Unitaire</th>
							<th>Quantite</th>
							<th>Editer</th>
							<th>Supprimer</th>
						</tr>
						<c:forEach items="${model.produits}" var="p">
							<tr>
								<td>${p.id}</td>
								<td>${p.des}</td>
								<td>${p.pu}</td>
								<td>${p.qte}</td>
								<td><a href="edit.do?id=${p.id}" class="btn btn-success">Editer</a></td>
								<td><a onclick="return confirm('etes-vous sur de vouloir supprimer ce produit ?')" href="supp.do?id=${p.id}" class="btn btn-danger" >Supprimer</a></td>
							</tr>
						</c:forEach>
						
					</table>
					</div>
				</div>
			</div>
		</div>
</body>
</html>