package ejb;


import javax.ejb.Stateless;

/**
 * Session Bean implementation class vol
 */
@Stateless
public class vol {
	
	private String codeVol;
	private String villeDepart;
	private String villeArrivee;
	private String dateVol;
	private String heureDepart;
	private String heureArrivee;
	private float tarif;
	private int nbPassagers;

    /**
     * Default constructor. 
     */
    public vol() {
    	this.codeVol = "a11541";
		this.villeDepart = "dakar";
		this.villeArrivee = "paris";
		this.dateVol = "02/07/2017";
		this.heureDepart = "12 : 15";
		this.heureArrivee = "18 : 15";
		this.tarif = 350000;
		this.nbPassagers = 300;
    }

	

	public String getCodeVol() {
		return codeVol;
	}

	public void setCodeVol(String codeVol) {
		this.codeVol = codeVol;
	}

	public String getVilleDepart() {
		return villeDepart;
	}

	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}

	public String getVilleArrivee() {
		return villeArrivee;
	}

	public void setVilleArrivee(String villeArrivee) {
		this.villeArrivee = villeArrivee;
	}

	public String getDateVol() {
		return dateVol;
	}

	public void setDateVol(String dateVol) {
		this.dateVol = dateVol;
	}

	public String getHeureDepart() {
		return heureDepart;
	}

	public void setHeureDepart(String heureDepart) {
		this.heureDepart = heureDepart;
	}

	public String getHeureArrivee() {
		return heureArrivee;
	}

	public void setHeureArrivee(String heureArrivee) {
		this.heureArrivee = heureArrivee;
	}

	public float getTarif() {
		return tarif;
	}

	public void setTarif(float tarif) {
		this.tarif = tarif;
	}

	public int getNbPassagers() {
		return nbPassagers;
	}

	public void setNbPassagers(int nbPassagers) {
		this.nbPassagers = nbPassagers;
	}
    

}
