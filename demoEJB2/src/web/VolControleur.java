package web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.vol;


@WebServlet("/VolControleur")
public class VolControleur extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private vol volDetails ;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("vol", volDetails);
		request.getRequestDispatcher("vol.jsp").forward(request, response);
	}

	

}
