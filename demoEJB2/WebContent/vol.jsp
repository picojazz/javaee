<%@page import="ejb.vol"%>
<% 
	vol vol = (vol)request.getAttribute("vol");
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>detail vol</title>
</head>
<body>
	<h1>Details du vol</h1>

	<fieldset>
		<table>
			<tr>
				<th>code du vol :</th>
				<td><%= vol.getCodeVol() %></td>
			</tr>
			<tr>
				<th>ville de depart :</th>
				<td><%= vol.getVilleDepart() %></td>
			</tr>
			<tr>
				<th>vile d'arrivee :</th>
				<td><%= vol.getVilleArrivee() %></td>
			</tr>
			<tr>
				<th>date du vol :</th>
				<td><%= vol.getDateVol() %></td>
			</tr>
			<tr>
				<th>heure de depart :</th>
				<td><%= vol.getHeureDepart() %></td>
			</tr>
			<tr>
				<th>heure d'arrivee :</th>
				<td><%= vol.getHeureArrivee() %></td>
			</tr>
			<tr>
				<th>tarif :</th>
				<td><%= vol.getTarif() %></td>
			</tr>
			<tr>
				<th>nombre de passagers :</th>
				<td><%= vol.getNbPassagers() %></td>
			</tr>

			
			
		</table>
	</fieldset>
	
	
</body>
</html>