package com.ProduitMVC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.ProduitMVC.dao.ProduitRepository;
import com.ProduitMVC.entities.Produit;

@SpringBootApplication
public class ProduitMvcApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ProduitMvcApplication.class, args);
		ProduitRepository pr = ctx.getBean(ProduitRepository.class);
		
		/*pr.save(new Produit("savon",100,35));
		pr.save(new Produit("huile",500,25));
		pr.save(new Produit("lait",250,38));
		pr.save(new Produit("beurre",400,90));*/
		
		pr.findAll().forEach(p -> System.out.println(p.toString()));
	}
}
