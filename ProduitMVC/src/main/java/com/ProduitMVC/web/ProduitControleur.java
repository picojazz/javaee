package com.ProduitMVC.web;

import java.util.List;

import javax.naming.Name;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ProduitMVC.dao.ProduitRepository;
import com.ProduitMVC.entities.Produit;


@Controller
public class ProduitControleur {
	@Autowired
	private ProduitRepository pr;
	
	@RequestMapping(value="/produits")
	public String produits(Model model , @RequestParam(name="p",defaultValue="0")int p,
			@RequestParam(name="s",defaultValue="6")int s,
			@RequestParam(name="motCle",defaultValue="")String mc){
		Page<Produit> produits = pr.chercherParMc("%"+mc+"%",new PageRequest(p, s));
		int[] pages = new int[produits.getTotalPages()];
		model.addAttribute("produits", produits);
		model.addAttribute("pages", pages);
		model.addAttribute("mc", mc);
		model.addAttribute("pc", p);
		return "produits";
	}
	@RequestMapping(value="/delete")
	public String delete(@RequestParam(name="p",defaultValue="0")int p,
			@RequestParam(name="id",defaultValue="")Long id,
			@RequestParam(name="motCle",defaultValue="")String mc){
		pr.delete(id);
		return "redirect:/produits.html?p="+p+"&motCle="+mc;
	}
	
}
