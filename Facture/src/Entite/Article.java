package Entite;

public class Article {
	private String libelle;
	private double quantite;
	private double prix;
	public Article(String libelle, double quantite, double prix) {
		super();
		this.libelle = libelle;
		this.quantite = quantite;
		this.prix = prix;
	}
	public Article() {
		super();
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public double getQuantite() {
		return quantite;
	}
	public void setQuantite(double quantite) {
		this.quantite = quantite;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	
	

}
