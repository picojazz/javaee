package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entite.Article;
@WebServlet({"/facture"})
public class Facture extends HttpServlet{
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		request.getRequestDispatcher("article.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String libelle = request.getParameter("libelle");
		double pu = Float.parseFloat(request.getParameter("pu"));
		double qte = Float.parseFloat(request.getParameter("qte"));
		if (pu <= 0 || qte <= 0) {
			String err ="le prix ou la quantite est negatif ou egal a 0";
			request.setAttribute("err", err);
			request.getRequestDispatcher("article.jsp").forward(request, response);
		}else{
			Article article = new Article(libelle,qte,pu);
			double montant = article.getPrix()*article.getQuantite(); 
			double remise ;
			if (montant <= 5000) {
				remise=0;
			}else if (montant > 5000 && montant <= 15000 ) {
				remise=3;
			}else if (montant > 15000 && montant < 30000 ) {
				remise=7;
			}else if (montant >= 30000 && montant <= 50000 ) {
				remise=12;
			}else{
				remise=20;
			}
			double montRemise = ((montant*remise)/100);
			double net = montant - montRemise;
			request.setAttribute("article", article);
			request.setAttribute("remise", remise);
			request.setAttribute("montRemise", montRemise);
			request.setAttribute("net", net);
			request.setAttribute("montant", montant);
			request.getRequestDispatcher("facture.jsp").forward(request, response);
		}
	}

}
