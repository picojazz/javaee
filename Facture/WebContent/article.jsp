<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Article</title>
</head>
<body>
    
    <div class="container ">
        <h1 class="text-center">Ajout d'un article </h1>
        <%if(request.getAttribute("err") != null){
        	%>

<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Erreur de saisie</strong> <%= request.getAttribute("err")%>
</div>

        	<%} %>
        <div class=" col-md-6 col-md-offset-3">
            
            <div class="panel panel-primary">
                  <div class="panel-heading">
                              
                        </div>
                  <div class="panel-body">
                        
                        
                        <form action="facture" method="POST" >
                            
                        
                            <div class="form-group">
                                <label for="">Libelle du produit :</label>
                                <input type="text" class="form-control" name="libelle" required>
                            </div>
                            <div class="form-group">
                                <label for="">Prix unitaire :</label>
                                <input type="text" class="form-control" name="pu" required>
                            </div>
                            <div class="form-group">
                                <label for="">quantite :</label>
                                <input type="text" class="form-control" name="qte" required>
                            </div>
                        
                            
                        
                            <button type="submit" class="btn btn-lg btn-primary">Ajouter</button>
                        </form>
                        
                        
                  </div>
            </div>
            
        </div>
        
    </div>
    
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>