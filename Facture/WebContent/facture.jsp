<%@page import="Entite.Article"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% Article a = (Article)request.getAttribute("article"); 
double remise = (double)request.getAttribute("remise");
double montRemise = (double)request.getAttribute("montRemise"); 
double net = (double)request.getAttribute("net");
double montant = (double)request.getAttribute("montant");
	
%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>facture</title>
</head>
<body>
    
    <div class="container ">
        <h1 class="text-center">Facture</h1>
        
        <div class=" col-md-8 col-md-offset-2">
            
            <div class="panel panel-primary">
                  <div class="panel-heading">
                              details
                        </div>
                  <div class="panel-body">
                        
                        
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Libelle</th>
                                    <th>Prix Unitaire</th>
                                    <th>Quantite</th>
                                    <th>Montant</th>
                                    <th>Remise(<%= remise%>%)</th>
                                    <th>Net a payer</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><%= a.getLibelle() %></td>
                                    <td><%= a.getPrix() %></td>
                                    <td><%= a.getQuantite() %></td>
                                    <td><%= montant %></td>
                                    <td><%= montRemise %></td>
                                    <td><%= net %></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        
                  </div>
            </div>
            
        </div>
        
    </div>
    
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>