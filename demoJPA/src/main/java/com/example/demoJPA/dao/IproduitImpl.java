package com.example.demoJPA.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.example.demoJPA.entities.Produit;
@Repository
@Transactional
public class IproduitImpl implements IProduit{
	@PersistenceContext
	EntityManager em;

	@Override
	public Produit ajouter(Produit p) {
		em.persist(p);
		return p;
	}

	@Override
	public Produit modifier(Produit p) {
		em.merge(p);
		return p;
	}

	

	@Override
	public List<Produit> lister() {
		
		Query req= em.createQuery("select p from Produit p") ;
		return req.getResultList();
	}

	@Override
	public Produit chercherParId(Long id) {
		Produit p = em.find(Produit.class, id);
		return p;
	}

	@Override
	public List<Produit> chercherParDesignation(String designation) {
		Query req = em.createQuery("select p from Produit p where p.designation like :x");
		req.setParameter("x", "%"+designation+"%");
		 
		return req.getResultList();
	}

	@Override
	public void supprimer(Long id) {
		Produit p = em.find(Produit.class, id);
		em.remove(p);
		
	}

}
