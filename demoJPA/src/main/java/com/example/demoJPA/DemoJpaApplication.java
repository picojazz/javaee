package com.example.demoJPA;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.example.demoJPA.dao.IProduit;
import com.example.demoJPA.dao.IproduitImpl;
import com.example.demoJPA.entities.Produit;

@SpringBootApplication
public class DemoJpaApplication {

	public static void main(String[] args) {
	ApplicationContext ctx = SpringApplication.run(DemoJpaApplication.class, args);
		IproduitImpl produitDao = (IproduitImpl) ctx.getBean(IProduit.class); 
		produitDao.ajouter(new Produit("savon",300,18)).toString();
		produitDao.ajouter(new Produit("bic",200,32)).toString();
		produitDao.ajouter(new Produit("ecouteur",250,12)).toString();
		produitDao.ajouter(new Produit("ttt",2500,120)).toString();
		long l= 2;
		produitDao.supprimer(l);
		List<Produit> p = produitDao.lister();
		/*List<Produit> p = produitDao.chercherParDesignation("t");*/
		for (Produit produit : p) {
			System.out.println(produit.toString());
		}
		/*long l= 4;
		System.out.println(produitDao.chercherParId(l));*/
		
	}
}
